require('dotenv').config();
const connect = require('./connect');

const User = require('./src/models/user.model')();
const Game = require('./src/models/game.model')() ;
const Article = require('./src/models/article.model')();

const neo = require('./neo');

// connect to the databases
connect.mongo(process.env.MONGO_TEST_DB);
connect.neo(process.env.NEO4J_TEST_DB);

beforeEach(async () => {
    // drop both collections before each test
    await Promise.all([User.deleteMany(), Game.deleteMany(), Article.deleteMany()]);

    // clear neo db before each test
    const session = neo.session();
    await session.run(neo.dropAll);
    await session.close();
});