const neo4j = require('neo4j-driver');

function connect(dbName) {
    this.dbName = dbName
    this.driver = neo4j.driver(
        process.env.NEO4J_URL,
        neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASSWORD)
    )
}

function session() {
    return this.driver.session();
    // When passing this into the session(), it gives me an error: Neo.ClientError.Database.DatabaseNotFound
    // {
    //     database: this.dbName,
    //     defaultAccessMode: neo4j.session.WRITE
    // }
}

module.exports = {
    connect,
    session,
    dropAll: 'MATCH (n) DETACH DELETE n',
    deleteUser: 'MATCH(usr:User{id: $userId}) DETACH DELETE usr',
    deleteGame: 'MATCH(game:Game{id: $gameId}) DETACH DELETE game',
    getFriends: 'MATCH(:User{id: $userId})-[:FRIENDS_WITH]-(friends:User) RETURN collect(DISTINCT friends.id) as friendIds',
    getFriendRequests: 'MATCH(:User{id: $userId})<-[:PENDING_REQUEST]-(pending:User) RETURN collect(DISTINCT pending.id) as pendingIds',
    addFriend: 'MERGE (user:User {id: $userId}) MERGE (friend:User {id: $friendId}) MERGE (user)-[:FRIENDS_WITH]->(friend)', 
    removeFriend: 'MATCH(:User{id: $userId})-[relation:FRIENDS_WITH]-(:User {id: $friendId}) DELETE(relation)', 
    sendFriendRequest: 'MERGE(user:User {id: $userId}) MERGE(sender:User {id: $senderId}) MERGE(sender)-[:PENDING_REQUEST]->(user)',
    removeFriendRequest: 'MATCH(:User {id: $userId})-[relation:PENDING_REQUEST]-(:User {id: $senderId}) DELETE(relation)',
    purchase: 'MERGE (game:Game {id:$gameId}) MERGE(category:Category {category:$category}) MERGE (user:User {id: $userId}) MERGE (user)-[:BOUGHT]->(game) MERGE (game)-[:IN_CATEGORY]->(category)',
    recommendSimilar: 'MATCH(usr:User{id:$userId})-[:BOUGHT]->(game:Game)<-[:BOUGHT]-(other:User)-[:BOUGHT]->(recGames:Game) WHERE (game)-[:IN_CATEGORY]->(:Category)<-[:IN_CATEGORY]-(recGames) RETURN recGames AS gameIds',
    recommendFriends: 'MATCH(usr:User {id: $userId})-[:FRIENDS_WITH*2]-(other:User) WHERE NOT (usr)-[:FRIENDS_WITH]-(other) RETURN collect(DISTINCT other.id) as friendIds'
}