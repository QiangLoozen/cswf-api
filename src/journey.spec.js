const chai = require('chai')
const expect = chai.expect

const requester = require('../requester.spec');

describe('user journeys', function() {
    it('create game; create user; user buys game; user leaves review; delete user; remove all reviews from user', async function() {
        let res;

        const testUser = {
            username: "Qiang",
            email: "qiang@gmail.com",
            password: "password",
            birthdate: new Date(Date.parse('04 Dec 1995 00:12:00 GMT')),
            role: "customer"
        }

        res = await requester.post('/api/register').send(testUser) // REGISTER USER
        expect(res).to.have.status(201)
        testUser.id = res.body._id
        const token = res.body.token
        const testGame = {
            name: 'Rust',
            price: 30,
            short_description: "short description",
            long_description: "long description",
            release_date: new Date(),
            trailer_url: "url",
            category: "action",
            publisher: "Facepunch",
            createdBy: "61aa2bbb3cbd1f8c8afd1a8f"
        }
        res = await requester.post('/api/game').send(testGame).set('Authorization', 'Bearer ' + token); // CREATE GAME
        expect(res).to.have.status(201)
        testGame.id = res.body._id;

        res = await requester.post(`/api/game/${testGame.id}/purchase`).send({userId: testUser.id}) // PURCHASE GAME
        expect(res).to.have.status(201)

        const testReview = {
            user: {_id: testUser.id},
            title: "Good game",
            content: "Nice game",
            rating: 4
        }
        res = await requester.post(`/api/game/${testGame.id}/reviews`).send(testReview).set('Authorization', 'Bearer ' + token); // POST REVIEW
        expect(res).to.have.status(201)

        res = await requester.get(`/api/game/${testGame.id}`) // CHECK GAME
        expect(res).to.have.status(200)
        expect(res.body).to.have.property('name', testGame.name)
        expect(res.body).to.have.property('short_description', testGame.short_description)
        expect(res.body).to.have.property('price', testGame.price)
        expect(res.body).to.have.property('reviews').and.to.have.length(1)
        const gameReview = res.body.reviews[0]
        expect(gameReview).to.have.property('rating', testReview.rating)
        expect(gameReview).to.have.property('content', testReview.content)
        expect(gameReview.user._id.toString()).to.equal(testUser.id)

        res = await requester.get(`/api/user/${testUser.id}`).set('Authorization', 'Bearer ' + token); // CHECK USER
        expect(res).to.have.status(200)
        expect(res.body).to.have.property('username', testUser.username)
        expect(res.body).to.have.property('games').and.to.be.length(1)
        const userReview = res.body.games[0]
        expect(userReview).to.have.property('name', testGame.name)
        expect(userReview).to.have.property('short_description', testGame.short_description)
        expect(userReview).to.have.property('price', testGame.price)

        res = await requester.delete(`/api/user/${testUser.id}`).set('Authorization', 'Bearer ' + token); // DELETE USER
        expect(res).to.have.status(204)


        res = await requester.get(`/api/game/${testGame.id}`) // CHECK GAME REVIEWS
        expect(res).to.have.status(200)
        expect(res.body).to.have.property('name', testGame.name)
        expect(res.body).to.have.property('short_description', testGame.short_description)
        expect(res.body).to.have.property('price', testGame.price)
        expect(res.body).to.have.property('reviews').and.to.have.length(0)


        
    })
    it('create game; create user; user creates a list; user add a game to list', async function() {
        let res;

        const testUser = {
            username: "Qiang",
            email: "qiang@gmail.com",
            password: "password",
            birthdate: new Date(Date.parse('04 Dec 1995 00:12:00 GMT')),
            role: "customer"
        }

        res = await requester.post('/api/register').send(testUser)
        expect(res).to.have.status(201)
        testUser.id = res.body._id
        const token = res.body.token;
        const testGame = {
            name: 'Rust',
            price: 30,
            short_description: "short description",
            long_description: "long description",
            release_date: new Date(),
            trailer_url: "url",
            category: "action",
            publisher: "Facepunch",
            createdBy: "61aa2bbb3cbd1f8c8afd1a8f"
        }
        res = await requester.post('/api/game').send(testGame).set('Authorization', 'Bearer ' + token);
        expect(res).to.have.status(201)
        testGame.id = res.body._id;

        const testList = {
            "name": "myList",
            "description": "My description"
        }

        res = await requester.post(`/api/user/${testUser.id}/lists`).send(testList).set('Authorization', 'Bearer ' + token)
        expect(res).to.have.status(201)
        expect(res.body).to.have.property('games').and.is.empty;
        expect(res.body.lists[0]).to.have.property('name', testList.name);
        testList.id = res.body.lists[0]._id;

        res = await requester.post(`/api/user/${testUser.id}/lists/${testList.id}/addGame/${testGame.id}`).set('Authorization', 'Bearer ' + token);
        expect(res).to.have.status(201)
        expect(res.body).to.have.property('lists').and.to.have.length(1);


        res = await requester.get(`/api/game/${testGame.id}`)
        expect(res).to.have.status(200)
        expect(res.body).to.have.property('name', testGame.name)
        expect(res.body).to.have.property('short_description', testGame.short_description)
        expect(res.body).to.have.property('price', testGame.price)

        res = await requester.get(`/api/user/${testUser.id}`).set('Authorization', 'Bearer ' + token);
        expect(res).to.have.status(200)
        expect(res.body).to.have.property('username', testUser.username)
    })
})