const express = require('express');
const router = express.Router();

const Article = require('../models/article.model')()

// Controllers
const GenericController = require('../controllers/generic.controller');
const crudController = new GenericController(Article);
const authController = require('../controllers/authentication.controller');
const articleController = require('../controllers/article.controller');
// Routes
router.post('/', authController.validateToken,  crudController.create);

router.get('/', crudController.getAll);

router.get('/:id', crudController.getOne);

router.put('/:id', authController.validateToken, articleController.updateArticle);

router.delete('/:id', authController.validateToken, articleController.deleteArticle);

module.exports = router;
