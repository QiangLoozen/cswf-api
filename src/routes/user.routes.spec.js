const chai = require('chai')
const expect = chai.expect

const requester = require('../../requester.spec')

const User = require('../models/user.model')();
const Game = require('../models/game.model')();

describe('user endpoints', function() {
    describe('integration tests', function() {
        let user1;
        let user2;
        let token;
        beforeEach( async () => {
            user1 = {username: 'Jane', email: 'jane@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"};
            user2 = {username: 'Joe', email: 'joe@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"};

            const res = await requester.post('/api/register').send({username: 'test', email: 'test@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "creator"})
            token = res.body.token
        });
    
        it('(GET /api/user) should give all users', async function() {
            await new User(user1).save();
            await new User(user2).save();
    
            const res = await requester.get('/api/user').set('Authorization', 'Bearer ' + token);
    
            expect(res).to.have.status(200)
            const users = res.body
            users.sort((lhs, rhs) => lhs.username < rhs.username ? -1 : 1)
            expect(users).to.have.length(3)
            expect(users[0]).to.have.property('username', user1.username)
            expect(users[0]).to.have.property('games').and.to.be.empty
            expect(users[1]).to.have.property('username', user2.username)
            expect(users[1]).to.have.property('games').and.to.be.empty
        })
    
        it('(GET /api/user) should give an user', async function() {
            const jane = new User(user1);
            await jane.save();

            const res = await requester.get(`/api/user/${jane.id}`).set('Authorization', 'Bearer ' + token);
            
            expect(res).to.have.status(200)
            expect(res.body).to.have.property('username', user1.username)
            expect(res.body).to.have.property('games').and.to.be.empty
        })
    
        it('(DELETE /api/user/:id) should delete an user', async function() {
            const res = await requester.post('/api/register').send(user1);
            const jane = await User.findById(res.body._id);
    
            const res2 = await requester.delete(`/api/user/${jane.id}`).set('Authorization', 'Bearer ' + res.body.token);
    
            expect(res2).to.have.status(204)
    
            const user = await User.findOne({username: user1.username})
            expect(user).to.be.null
        })

        it('(GET /api/user/:id/lists) should get all lists of an user', async function() {
            const res = await requester.post('/api/register').send(user1);
            const jane = await User.findById(res.body._id);
            jane.lists = [{name: "myList", description: "desc"}]
            await jane.save();
    
            const res2 = await requester.get(`/api/user/${jane.id}/lists`).set('Authorization', 'Bearer ' + res.body.token)
    
            expect(res2).to.have.status(200)
    
            const user = await User.findOne({username: "Jane"})
            expect(user.lists).to.have.length(1);
            expect(user.lists[0].name).equals("myList");
        })

        it('(POST /api/user/:id/lists) should create a new list for an user', async function() {
            const res = await requester.post('/api/register').send(user1);
            const jane = await User.findById(res.body._id);
    
            const res2 = await requester.post(`/api/user/${jane.id}/lists`).send({name: "myList", description: "desc"}).set('Authorization', 'Bearer ' + res.body.token);
    
            expect(res2).to.have.status(201)
    
            const user = await User.findOne({username: "Jane"})
            expect(user.lists).to.have.length(1);
            expect(user.lists[0].name).equals("myList");
        })

        it('(POST /api/user/:id/lists) should not create a list without a name', async function() {
            const res = await requester.post('/api/register').send(user1);
            const jane = await User.findById(res.body._id);

            const res2 = await requester.post(`/api/user/${jane.id}/lists`).send({description: "desc"}).set('Authorization', 'Bearer ' + res.body.token);
    
            expect(res2).to.have.status(400)
    
            const user = await User.findOne({username: "Jane"})
            expect(user.lists).to.be.empty
        })

        it('(DELETE /api/user/:id/lists/:listId) should delete a list of an user', async function() {
            const res = await requester.post('/api/register').send(user1);
            const jane = await User.findById(res.body._id);

            jane.lists = [{name: "myList", description: "desc"}]
            await jane.save();
    
            const res2 = await requester.delete(`/api/user/${jane.id}/lists/${jane.lists[0].id}`).set('Authorization', 'Bearer ' + res.body.token);
    
            expect(res2).to.have.status(204)
    
            const user = await User.findOne({username: "Jane"})
            expect(user.lists).to.be.empty;
        })

        it('(UPDATE /api/user/:id/lists/:list) should update a list of an user', async function() {
            const res = await requester.post('/api/register').send(user1);
            const jane = await User.findById(res.body._id);

            jane.lists = [{name: "myList", description: "desc"}]
            await jane.save();

            const res2 = await requester.put(`/api/user/${jane.id}/lists/${jane.lists[0].id}`).send({name: "newListName", description: "newDescription"}).set('Authorization', 'Bearer ' + res.body.token);
    
            expect(res2).to.have.status(204)
    
            const user = await User.findOne({username: "Jane"})
            expect(user.lists).to.have.length(1);
            expect(user.lists[0].name).equals("newListName");
        })

        it('(POST /api/user/:id/lists/:list/addGame/:gameId) should add a game to a list', async function() {
            const res = await requester.post('/api/register').send(user1);
            const jane = await User.findById(res.body._id);

            jane.lists = [{name: "myList", description: "desc"}]
            await jane.save();

            const newGame = new Game({name: "n", price: 0, short_description: "sd", long_description: "ld", release_date: new Date(), trailer_url: "tu", category: "action", publisher: "p", createdBy: jane._id.toString()});
            await newGame.save();

            const res2 = await requester.post(`/api/user/${jane.id}/lists/${jane.lists[0].id}/addGame/${newGame.id}`).set('Authorization', 'Bearer ' + res.body.token);
    
            expect(res2).to.have.status(201)
    
            const user = await User.findOne({username: "Jane"})
            expect(user.lists).to.have.length(1);
            expect(user.lists[0].name).equals("myList");
            expect(user.lists[0].games[0]._id.toString()).equals(newGame.id);
        })

        it('(DELETE /api/user/:id/lists/:list/removeGame/:gameId) should remove a game from a list', async function() {
            const res = await requester.post('/api/register').send(user1);
            const jane = await User.findById(res.body._id);

            const newGame = new Game({name: "n", price: 0, short_description: "sd", long_description: "ld", release_date: new Date(), trailer_url: "tu", category: "action", publisher: "p", createdBy: jane._id.toString()});
            await newGame.save();
            
            jane.lists = [{name: "myList", description: "desc"}]
            jane.lists[0].games.push(newGame);

            await jane.save();

            const res2 = await requester.delete(`/api/user/${jane.id}/lists/${jane.lists[0].id}/removeGame/${newGame.id}`).set('Authorization', 'Bearer ' + res.body.token);
    
            expect(res2).to.have.status(204)
    
            const user = await User.findOne({username: "Jane"})
            expect(user.lists).to.have.length(1);
            expect(user.lists[0].name).equals("myList");
            expect(user.lists[0].games).to.be.empty
        })
    })
})