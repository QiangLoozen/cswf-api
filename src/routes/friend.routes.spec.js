const chai = require('chai')
const expect = chai.expect

const requester = require('../../requester.spec');
const neo = require('../../neo');

const User = require('../models/user.model')();

describe('friend endpoints', function() {
    describe('integration tests', function() {
        let user1;
        let user2;
    
        beforeEach((done) => {
            user1 = {username: 'Jane', email: 'jane@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"};
            user2 = {username: 'Joe', email: 'joe@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"};
            done();
        });
    
        it('(POST /api/user/:id/friendRequest/decline) should decline a friendrequest', async function() {
            //Arrange
            const existingUser1 = new User(user1);
            const existingUser2 = new User(user2);
            Promise.all([existingUser1.save(), existingUser2.save()]);
            
            const session = neo.session();
            await session.run(neo.sendFriendRequest, {userId: existingUser2.id.toString(), userName: existingUser1.username , senderId: existingUser1.id.toString(), senderName: existingUser1.username});
            const res = await session.run(neo.getFriendRequests, {userId: existingUser2.id.toString() })
            expect(res.records[0].get('pendingIds')).to.have.length(1);
            
            // Act
            await session.run(neo.removeFriendRequest, {userId: existingUser2.id.toString(), senderId: existingUser1.id.toString() })
            const res2 = await session.run(neo.getFriendRequests, {userId: existingUser2.id.toString() })
            expect(res2.records[0].get('pendingIds')).to.have.length(0);
        })
    })

    describe('system tests', function() {
        let user1;
        let user2;
    
        beforeEach((done) => {
            user1 = {username: 'Jane', email: 'jane@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"};
            user2 = {username: 'Joe', email: 'joe@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"};
            done();
        });

        it('(POST /api/user/:id/friendRequest) should send a friendrequest and retrieve friendrequests', async function() {
            const resU1 = await requester.post('/api/register').send(user1);
            const resU2 = await requester.post('/api/register').send(user2);
            const existingUser1 = await User.findById(resU1.body._id);
            const existingUser2 = await User.findById(resU2.body._id);

            const res = await requester
                .post(`/api/user/${existingUser2.id}/friendRequest`).set('Authorization', 'Bearer ' + resU1.body.token)	
                .send({senderId: existingUser1.id});

            expect(res).to.have.status(204);

            
            const res2 = await requester
                .get(`/api/user/${existingUser2.id}/friendRequests`).set('Authorization', 'Bearer ' + resU2.body.token)	

            expect(res2).to.have.status(200);
            expect(res2.body).to.have.length(1);
            expect(res2.body[0]._id).to.equal(existingUser1.id.toString());

        })
        it('should accept a friendrequest, retrieve friends and remove the friend ', async function() {
            // Arrange
            const resU1 = await requester.post('/api/register').send(user1);
            const resU2 = await requester.post('/api/register').send(user2);
            const existingUser1 = await User.findById(resU1.body._id);
            const existingUser2 = await User.findById(resU2.body._id);

            const res = await requester
            .post(`/api/user/${existingUser2.id}/friendRequest`).set('Authorization', 'Bearer ' + resU1.body.token)	
            .send({senderId: existingUser1.id});

            expect(res).to.have.status(204);

            // Act
            const res2 = await requester
                .post(`/api/user/${existingUser2.id}/friendRequest/accept`).set('Authorization', 'Bearer ' + resU2.body.token)	
                .send({senderId: existingUser1.id});
            
            expect(res2).to.have.status(204);
            
            const res3 = await requester
                .get(`/api/user/${existingUser2.id}/friends`).set('Authorization', 'Bearer ' + resU2.body.token);

            expect(res3).to.have.status(200);
            expect(res3.body).to.have.length(1);
            expect(res3.body[0]._id).to.equal(existingUser1.id.toString());

            const res4 = await requester
                .post(`/api/user/${existingUser2.id}/removeFriend`).set('Authorization', 'Bearer ' + resU2.body.token)
                .send({senderId: existingUser1.id});
            
            expect(res4).to.have.status(204);
        })
    })
})