const express = require('express');
const router = express.Router();

const User = require('../models/user.model')();

// Controllers
const GenericController = require('../controllers/generic.controller');
const crudController = new GenericController(User);
const userController =  require('../controllers/user.controller');
const userListController =  require('../controllers/userList.controller');
const authController = require('../controllers/authentication.controller');

// User routes
router.get('/', authController.validateToken, crudController.getAll);

router.get('/emails', userController.getAllEmails);

router.get('/:id',authController.validateToken, userController.getUser);

router.put('/:id', authController.validateToken, userController.updateUser);

router.delete('/:id', authController.validateToken, userController.deleteUser);

// User list routes
router.get('/:id/lists', authController.validateToken, userListController.getLists)
router.post('/:id/lists', authController.validateToken,  userListController.createList);
router.put('/:id/lists/:listId', authController.validateToken,  userListController.updateList);
router.delete('/:id/lists/:listId', authController.validateToken,  userListController.deleteList);
router.post('/:id/lists/:listId/addGame/:gameId', authController.validateToken, userListController.addToList);
router.delete('/:id/lists/:listId/removeGame/:gameId', authController.validateToken,  userListController.removeFromList);


module.exports = router;