const express = require('express');
const router = express.Router();

// Controller
const friendController =  require('../controllers/friend.controller');
const authController = require('../controllers/authentication.controller');
// Routes
router.get('/:id/friends', authController.validateToken, friendController.getFriends)
router.get('/:id/friendRequests',authController.validateToken, friendController.getFriendRequests)
router.post('/:id/friendRequest', authController.validateToken, friendController.friendRequest)
router.post('/:id/friendRequest/accept', authController.validateToken, friendController.acceptFriendRequest)
router.post('/:id/friendRequest/decline', authController.validateToken, friendController.declineFriendRequest)
router.post('/:id/removeFriend', authController.validateToken, friendController.removeFriend) //Param: id deletes senderId in body

module.exports = router;