const express = require("express");
const router = express.Router();

// Controllers
const authenticationController = require('../controllers/authentication.controller');

// Routes
router.post('/register', authenticationController.register);
router.post('/login', authenticationController.login);

module.exports = router;