const chai = require('chai')
const expect = chai.expect

const requester = require('../../requester.spec')

const User = require('../models/user.model')()


describe('authentication endpoints', function() {

    describe('integration tests', function() {  
        let user1;
        let user2;
        beforeEach((done) => {
            user1 = {username: 'Jane', email: 'jane@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"};
            user2 = {username: 'Joe', email: 'joe@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"};
            done();
        });

        it('(POST /api/register) should create a new user', async function() {
            const res = await requester.post('/api/register').send(user1)
            expect(res).to.have.status(201)
            expect(res.body).to.have.property('_id')
            expect(res.body).to.have.property('token')
            
            const user = await User.findOne({name: "Jane"})
            expect(user).to.have.property('username', user1.username)
            expect(user).to.have.property('games').and.to.be.empty
        })
    
        it('(POST /api/register) should not create a user without a name', async function() {
            user1.username = undefined
            const res = await requester.post('/api/register').send(user1)
            expect(res).to.have.status(400)
            
            const count = await User.find().countDocuments()
            expect(count).to.equal(0);
        })
    })

    describe('system tests', function() {
        let user1;
        let user2;
        let token;
        beforeEach(async () => {
            user1 = {username: 'Jane', email: 'jane@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"};
            user2 = {username: 'Joe', email: 'joe@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"};
            const res = await requester.post('/api/register').send({username: 'test', email: 'test@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "creator"})
            token = res.body.token
        });
        
        it('should create two users and retrieve a list of users', async function() {
            const res1 = await requester.post('/api/register').send(user1)
            expect(res1).to.have.status(201)
            expect(res1.body).to.have.property('_id')
            expect(res1.body).to.have.property('token')
            expect(res1.body).to.have.property('username', user1.username)

            const res2 = await requester.post('/api/register').send(user2)
            expect(res2).to.have.status(201)
            expect(res2.body).to.have.property('_id')
            expect(res2.body).to.have.property('token')
            expect(res2.body).to.have.property('username', user2.username)

            const res3 = await requester.get('/api/user').set('Authorization', 'Bearer ' + token);
            expect(res3).to.have.status(200)
            res3.body.forEach((item) => {
                switch (item._id.toString()) {
                    case res1.body._id:
                        expect(item.username).to.equal(res1.body.username);
                        break;
                    case res2.body._id:
                        expect(item.username).to.equal(res2.body.username);
                        break;
                }
            })
        })

        it('should login with correct credentials, and give an error with invalid credentials', async function() {
            const res1 = await requester.post('/api/login').send({email: "nonExistant@gmail.com", password: user1.password})
            expect(res1).to.have.status(404)
            expect(res1.body).to.have.property('message', "Email does not exist!")

            // Register via register endpoint, because of bcrypt
            const res2 = await requester.post('/api/register').send(user1)
            expect(res2).to.have.status(201);

            const res3 = await requester.post('/api/login').send({email: user1.email, password: user1.password})

            expect(res3).to.have.status(200)
            expect(res3.body).to.have.property('_id')
            expect(res3.body).to.have.property('token')
            expect(res3.body).to.have.property('username', user1.username)
        })
    })
})