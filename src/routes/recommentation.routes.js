const express = require('express');
const router = express.Router();

const recommendationController = require('../controllers/recommendation.controller')

router.get('/user/:id/recommendations/similarGames', recommendationController.similarGames)
router.get('/user/:id/recommendations/recommendedFriends', recommendationController.recommendedFriends)

module.exports = router;