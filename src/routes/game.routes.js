const express = require('express');
const router = express.Router();

const Game = require('../models/game.model')();

// Controllers
const GenericController = require('../controllers/generic.controller');
const crudController = new GenericController(Game);
const gameController = require('../controllers/game.controller');
const authController = require('../controllers/authentication.controller');

// Routes
router.post('/', authController.validateToken, crudController.create);

router.get('/', crudController.getAll);

router.get('/:id', gameController.getGame);

router.put('/:id', authController.validateToken, gameController.updateGame);

router.delete('/:id', authController.validateToken, gameController.deleteGame);

router.post('/:id/purchase', gameController.purchase)


//reviews
router.post('/:id/reviews',  gameController.createReview)
router.put('/:id/reviews/:reviewId', gameController.editReview)
router.delete('/:id/reviews/:reviewId', gameController.deleteReview)


router.post('/:id/reviews/:reviewId/addLike', gameController.addLike)
router.post('/:id/reviews/:reviewId/removeLike', gameController.removeLike)
module.exports = router;