const chai = require('chai')
const expect = chai.expect

const requester = require('../../requester.spec')

const User = require('../models/user.model')()
const Article = require('../models/article.model')()


describe('article endpoints', function() {
    describe('integration tests', function() {
        let article1;
        let article2;
        let creator;
        let token;
        let id;
        beforeEach(async () => {
            creator = new User({username: 'Creatror', email: 'creator@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "creator"});
            creator.save().then(() => {
                article1 = {user: creator, title: "article1", subtitle: "subtitle1", content: "content1", category: "news"};
                article2 = {user: creator, title: "article2", subtitle: "subtitle2", content: "content2", category: "news"};
            })

            const res = await requester.post('/api/register').send({username: 'test', email: 'test@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "creator"})
            token = res.body.token
            id = res.body._id
        })

        it('(POST /api/article) should create a new article', async function() {
            const res = await requester.post('/api/article').send(article1).set('Authorization', 'Bearer ' + token);
            expect(res).to.have.status(201)
            expect(res.body).to.have.property('_id')
    
            const article = await Article.findOne({title: article1.title})
            expect(article).to.have.property('title', article1.title)
            expect(article).to.have.property('content', article1.content);
        })

        it('(POST /api/article) should create a new article', async function() {
            article1.title = undefined
            const res = await requester.post('/api/article').send(article1).set('Authorization', 'Bearer ' + token);
            expect(res).to.have.status(400)

            const count = await Article.find().countDocuments()
            expect(count).to.equal(0)
        })

        it('(POST /api/article) should NOT create a new article without authentication', async function() {
            const res = await requester.post('/api/article').send(article1);
            expect(res).to.have.status(401)

            const count = await Article.find().countDocuments()
            expect(count).to.equal(0)
        })
    
        it('(GET /api/article) should get all articles', async function() {
    
            await new Article(article1).save();
            await new Article(article2).save();
    
            const res = await requester.get('/api/article');
    
            expect(res).to.have.status(200)
            const articles = res.body
            articles.sort((lhs, rhs) => lhs.title < rhs.title ? -1 : 1)
            expect(articles).to.have.length(2)
            expect(articles[0]).to.have.property('title', article1.title)
            expect(articles[1]).to.have.property('title', article2.title)
        })

        it('(GET /api/article/:id) should get an article', async function() {
            
            const newArticle = new Article(article1);
            await newArticle.save();

            const res = await requester.get(`/api/article/${newArticle.id}`);

            expect(res).to.have.status(200)
            expect(res.body).to.have.property('title', newArticle.title)
        })

        it('(PUT /api/article/:id) should edit an article', async function() {
            const newArticle = new Article(article1);
            newArticle.user = id;
            await newArticle.save();
            
            let updatedData = article1;
            updatedData.title = "Updated Title"

            const res = await requester.put(`/api/article/${newArticle.id}`).send(updatedData).set('Authorization', 'Bearer ' + token);
    
            expect(res).to.have.status(204)
    
            const article = await Article.findOne({name: updatedData.title})
            expect(article).not.to.be.null;
        })

        it('(PUT /api/article/:id) should NOT edit an article from another user', async function() {
            const newArticle = new Article(article1);
            await newArticle.save();
            
            let updatedData = article1;
            updatedData.title = "Updated Title"

            const res = await requester.put(`/api/article/${newArticle.id}`).send(updatedData).set('Authorization', 'Bearer ' + token);
    
            expect(res).to.have.status(401)
            expect(res.body.message).to.equal("Unauthorized action.");
        })
    
    
        it('(DELETE /api/article/:id) should delete an article', async function() {
            const newArticle = new Article(article1);
            newArticle.user = id;
            await newArticle.save();
    
            const res = await requester.delete(`/api/article/${newArticle.id}`).set('Authorization', 'Bearer ' + token);
    
            expect(res).to.have.status(204)
    
            const article = await Article.findOne({name: newArticle.title})
            expect(article).to.be.null
        })

        it('(DELETE /api/article/:id) should NOT delete an for another user', async function() {
            const newArticle = new Article(article1);
            await newArticle.save();
    
            const res = await requester.delete(`/api/article/${newArticle.id}`).set('Authorization', 'Bearer ' + token);
    
            expect(res).to.have.status(401)
            expect(res.body.message).to.equal("Unauthorized action.");
        })
    })

    describe('system tests', function() {
        let article1;
        let article2;
        let creator;
        let token;
        beforeEach(async () => {
            creator = new User({username: 'Creatror', email: 'creator@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "creator"});
            creator.save().then(() => {
                article1 = {user: creator, title: "article1", subtitle: "subtitle1", content: "content1", category: "news"};
                article2 = {user: creator, title: "article2", subtitle: "subtitle2", content: "content2", category: "news"};

            })
            const res = await requester.post('/api/register').send({username: 'test', email: 'test@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "creator"})
            token = res.body.token
        })

        it('should create two articles and retrieve a list of articles', async function() {

            const res1 = await requester.post('/api/article').send(article1).set('Authorization', 'Bearer ' + token);
            expect(res1).to.have.status(201)
            expect(res1.body).to.have.property('_id')

            const res2 = await requester.post('/api/article').send(article2).set('Authorization', 'Bearer ' + token);
            expect(res2).to.have.status(201)
            expect(res2.body).to.have.property('_id')

            const res3 = await requester.get('/api/article')
            expect(res3).to.have.status(200)
            expect(res3.body).to.have.length(2)

            res3.body.forEach((item) => {
                switch (item._id.toString()) {
                    case res1.body._id:
                        expect(item.title).to.equal(res1.body.title);
                        break;
                    case res2.body._id:
                        expect(item.title).to.equal(res2.body.title);
                        break;
                }
            })
        })
    })
})