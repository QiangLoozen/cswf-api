const chai = require('chai')
const expect = chai.expect

const requester = require('../../requester.spec')

const Game = require('../models/game.model')();
const User = require('../models/user.model')();

describe('game endpoints', function() {
    describe('integration tests', function() {
        let game1;
        let game2;
        let token;
        let id;
        beforeEach(async () => {
            game1 = {name: "Rust", price: 23, short_description: "sd", long_description: "ld", release_date: new Date(), trailer_url: "tu", category: "action", publisher: "p", createdBy: "61a9086bd48cb5507177215a"};
            game2 = {name: "Minecraft", price: 23, short_description: "sd", long_description: "ld", release_date: new Date(), trailer_url: "tu", category: "action", publisher: "p", createdBy: "61a9086bd48cb5507177215a"};
            
            const res = await requester.post('/api/register').send({username: 'test', email: 'test@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "creator"})
            token = res.body.token;
            id = res.body._id;
        });
    
        it('(GET /api/game) should give all games', async function() {
            await new Game(game1).save();
            await new Game(game2).save();
            
            const res = await requester.get('/api/game')
    
            expect(res).to.have.status(200)
            const games = res.body
            games.sort((lhs, rhs) => lhs.name < rhs.name ? -1 : 1)
            expect(games).to.have.length(2)
            expect(games[0]).to.have.property('name', game2.name)
            expect(games[1]).to.have.property('name', game1.name)
        })

        it('(POST /api/game) should create a new game', async function() {
    
            const res = await requester.post('/api/game').send(game1).set('Authorization', 'Bearer ' + token);
    
            expect(res).to.have.status(201)

            const games = await Game.find();
            expect(games).to.have.length(1);
            expect(games[0].name).equal("Rust");
        })

        it('(POST /api/game) should not create a game without a name', async function() {
            game1.name = undefined;
            const res = await requester.post('/api/game').send(game1).set('Authorization', 'Bearer ' + token);
    
            expect(res).to.have.status(400)

            const count = await Game.find().countDocuments();
            expect(count).to.equal(0);
        })

        
        it('(DELETE /api/game/:id) should delete a game', async function() {
            const newGame = new Game(game1);
            newGame.createdBy = id;
            await newGame.save();

            const res = await requester.delete(`/api/game/${newGame.id}`).set('Authorization', 'Bearer ' + token);
    
            expect(res).to.have.status(204)

            const count = await Game.find().countDocuments();
            expect(count).to.equal(0);
        })

        it('(DELETE /api/game/:id) should NOT delete a game from another user', async function() {
            const newGame = new Game(game1);
            await newGame.save();

            const res = await requester.delete(`/api/game/${newGame.id}`).set('Authorization', 'Bearer ' + token);
    
            expect(res).to.have.status(401)
            expect(res.body.message).to.equal("Unauthorized action.");
        })

        it('(PUT /api/game/:id) should update a game', async function() {
            const newGame = new Game(game1);
            newGame.createdBy = id;
            await newGame.save();

            game1.name = "updatedGame"
            const res = await requester.put(`/api/game/${newGame.id}`).send(game1).set('Authorization', 'Bearer ' + token);
    
            expect(res).to.have.status(204)

            const games = await Game.find();
            expect(games).to.have.length(1);
            expect(games[0].name).to.equal("updatedGame");
        })

        it('(PUT /api/game/:id) should NOT update a game from another user', async function() {
            const newGame = new Game(game1);
            await newGame.save();

            game1.name = "updatedGame"
            const res = await requester.put(`/api/game/${newGame.id}`).send(game1).set('Authorization', 'Bearer ' + token);
    
            expect(res).to.have.status(401)
            expect(res.body.message).to.equal("Unauthorized action.");
        })

        it('(DELETE /api/game) should delete a review', async function() {
            const resU = await requester.post('/api/register').send({username: 'test', email: 'test9@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "creator"})
            const jane = await User.findById(resU.body._id);

            const review = {
                user: jane,
                title: "Best game ever",
                content: "Content",
                rating: 5
            }
            game1.reviews = [review];
            const existingGame = new Game(game1);
            await existingGame.save();
            
            const reviewId = existingGame.reviews[0].id

            const res = await requester.delete(`/api/game/${existingGame.id}/reviews/${reviewId}`).send(review).set('Authorization', 'Bearer ' + resU.body.token);
            expect(res).to.have.status(204);

            const reviews = await Game.findOne({id: existingGame.id});
            expect(reviews.reviews).to.have.length(0)
        })

        it('(PUT /api/game) should edit a review', async function() {
            const resU = await requester.post('/api/register').send({username: 'test', email: 'test7@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "creator"})
            const jane = await User.findById(resU.body._id);

            const review = {
                user: jane,
                title: "Best game ever",
                content: "Content",
                rating: 5
            }
            game1.reviews = [review];
            const existingGame = new Game(game1);
            await existingGame.save();
            
            const reviewId = existingGame.reviews[0].id

            review.title = "newTitle"
            review._id = reviewId

            const res = await requester.put(`/api/game/${existingGame.id}/reviews/${reviewId}`).send(review).set('Authorization', 'Bearer ' + resU.body.token);
            expect(res).to.have.status(204);

            const updatedReview = await Game.findOne({id: existingGame.id});
            expect(updatedReview.reviews).to.have.length(1)
            expect(updatedReview.reviews[0]).to.have.property("title", "newTitle");
        })

       
    })

    describe('system tests', function() {
        let game1;
        let game2;
        let token;
        beforeEach(async () => {
            game1 = {name: "Rust", price: 23, short_description: "sd", long_description: "ld", release_date: new Date(), trailer_url: "tu", category: "action", publisher: "p", createdBy: "61a9086bd48cb5507177215a"};
            game2 = {name: "Minecraft", price: 23, short_description: "sd", long_description: "ld", release_date: new Date(), trailer_url: "tu", category: "action", publisher: "p", createdBy: "61a9086bd48cb5507177215a"};
            const res = await requester.post('/api/register').send({username: 'test', email: 'test@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "creator"})
            token = res.body.token;
        });
        
        it('should create and retrieve a game', async function() {

            const res1 = await requester.post('/api/game').send(game1).set('Authorization', 'Bearer ' + token);
            expect(res1).to.have.status(201);
            expect(res1.body).to.have.property('_id');

            const id = res1.body._id;
            const res2 = await requester.get(`/api/game/${id}`);
            expect(res2).to.have.status(200);
            expect(res2.body).to.have.property('_id', id);
            expect(res2.body).to.have.property('name', game1.name);
            expect(res2.body).to.have.property('price', game1.price);
            expect(res2.body).to.have.property('short_description', game1.short_description);
            expect(res2.body).to.have.property('category', game1.category);
        })

        it('should create and retrieve reviews of a game', async function() {
            const existingGame = new Game(game1);
            await existingGame.save();

            const res = await requester.post('/api/register').send({username: 'test', email: 'test1@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "creator"})

            const jane = await User.findById(res.body._id);

            const reviewData = {
                user: jane,
                title: "Best game ever",
                content: "Content",
                rating: 5
            }

            const res1 = await requester.post(`/api/game/${existingGame.id}/reviews`).send(reviewData).set('Authorization', 'Bearer ' + res.body.token)	
            expect(res1).to.have.status(201);

            const res2 = await requester.get(`/api/game/${existingGame.id}`);
            expect(res2).to.have.status(200);
            expect(res2.body.reviews).to.have.length(1);
            expect(res2.body.reviews[0]).to.have.property("_id");
            expect(res2.body.reviews[0]).to.have.property("user");
            expect(res2.body.reviews[0].user).to.have.property("_id", jane.id);
            expect(res2.body.reviews[0]).to.have.property("title", reviewData.title);
            expect(res2.body.reviews[0]).to.have.property("content", reviewData.content);
        })
    })
})