const chai = require('chai')
const expect = chai.expect

const Game = require('../models/game.model')()
const User = require('../models/user.model')()

const requester = require('../../requester.spec')

const neo = require('../../neo')

describe('recommendation routes', () => {
    describe('integration tests', () => {
        let user1;
        let user2;
        let user3;
        beforeEach(async function() {
            const game1 = new Game({
                createdBy: "61a76293aa00c73ff7434f30", 
                name: "game1", 
                price: 10, 
                short_description: "sd", 
                long_description: "ld", 
                release_date: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), 
                trailer_url: "tu", 
                category: "action", 
                publisher: "p"
            });
            const game2 = new Game({
                createdBy: "61a76293aa00c73ff7434f30", 
                name: "game2", 
                price: 10, 
                short_description: "sd", 
                long_description: "ld", 
                release_date: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), 
                trailer_url: "tu", 
                category: "action", 
                publisher: "p"
            });

            user1 = new User({username: 'Jane', email: 'jane@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"});
            user2 = new User({username: 'Joe', email: 'joe@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"});
            user3 = new User({username: 'Jack', email: 'jack@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"});

            await Promise.all([game1.save(), game2.save(), user1.save(), user2.save(), user3.save()])

            const session = neo.session()
            await session.run(neo.purchase, {gameId: game1._id.toString(), gameName: game1.name, category: game1.category, userId: user1._id.toString(), userName: user1.username})
            await session.run(neo.purchase, {gameId: game2._id.toString(), gameName: game2.name, category: game2.category, userId: user1._id.toString(), userName: user1.username})
            await session.run(neo.purchase, {gameId: game2._id.toString(), gameName: game2.name, category: game2.category, userId: user2._id.toString(), userName: user2.username})
            
            await session.run(neo.addFriend, {userId: user2._id.toString(), userName: user2.username, friendId: user1._id.toString(), friendName: user1.username})
            await session.run(neo.addFriend, {userId: user3._id.toString(), userName: user3.username, friendId: user1._id.toString(), friendName: user1.username})
            session.close()
        })

        it('Should give game recommendations for user2', async function() {
            // Recommended = Look for similar purchases and bases on those purchases, look for the games other user bought in the same category 
            const res = await requester.get(`/api/user/${user2._id}/recommendations/similarGames`)

            expect(res.body).to.have.length(1)
            const names = res.body.map(game => game.name)
            expect(names).to.have.members(['game1'])
        })

        
        it('Should not give game recommentations for user1', async function() {
            // Recommended = Look for similar purchases and bases on those purchases, look for the games other user bought in the same category 
            const res = await requester.get(`/api/user/${user3._id}/recommendations/similarGames`)

            expect(res.body).to.have.length(0)
        })

                
        it('Should recommend Joe to Jack', async function() {
            // Friends of friends
            const res = await requester.get(`/api/user/${user3._id}/recommendations/recommendedFriends`)

            expect(res.body).to.have.length(1)
            expect(res.body[0].username).to.equal(user2.username);
        })

        it('Should recommend Jack to Joe', async function() {
            // Friends of friends
            const res = await requester.get(`/api/user/${user2._id}/recommendations/recommendedFriends`)

            expect(res.body).to.have.length(1)
            expect(res.body[0].username).to.equal(user3.username);
        })

        it('Should not recommend friends to Jane', async function() {
            // Friends of friends
            const res = await requester.get(`/api/user/${user1._id}/recommendations/recommendedFriends`)

            expect(res.body).to.have.length(0)
        })
    })
})