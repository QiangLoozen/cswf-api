class EntityNotFoundError extends Error {
    constructor(message) {
        super(message)
        this.name = 'EntityNotFoundError'
    }
}

class AuthenticationError extends Error {
    constructor(message) {
        super(message)
        this.name = 'AuthenticationError'
    }
}

function validation(err, req, res, next) {
    if (err.name === 'ValidationError') {
        res.status(400).json({
            message: err.message
        })
    } else {
        next(err)
    }
}

function duplicate(err, req, res, next) {
    if (err.name === 'MongoServerError' && err.code === 11000) {
        res.status(400).json({
            message: 'Duplicate key value!', 
            keyValue: err.keyValue
        })
    } else {
        next(err)
    }
}

function cast(err, req, res, next) {
    if (err.name === 'CastError') {
        res.status(400).json({
            message: `Invalid resource id: ${err.value}`
            // message: err
        })
    } else {
        next(err)
    }
}

function entityNotFound(err, req, res, next) {
    if (err.name === 'EntityNotFoundError') {
        res.status(404).json({
            message: err.message
        })
    } else {
        next(err)
    }
}

function authenticationFailed(err, req, res, next) {
    if (err.name === 'AuthenticationError') {
        res.status(401).json({
            message: err.message
        })
    } else {
        next(err)
    }
}

module.exports = {
    EntityNotFoundError,
    AuthenticationError,
    handlers: [
        validation,
        duplicate,
        cast,
        entityNotFound,
        authenticationFailed
    ],
}