const express = require('express');

require('express-async-errors'); // Catches exceptions in routers

const app = express();

const cors = require('cors');
const helmet = require('helmet');

const morgan = require('morgan');


app.use(express.json());

app.use(cors());

app.use(helmet());

app.use(morgan('dev'));

// Endpoints
const articleRoutes = require('./routes/article.routes');
const gameRoutes = require('./routes/game.routes');
const userRoutes = require('./routes/user.routes');
const friendRoutes = require('./routes/friend.routes');

const recommendationRoutes = require('./routes/recommentation.routes');
const authenticationRoutes = require('./routes/authentication.routes');

app.use('/api/game', gameRoutes); 
app.use('/api/user', userRoutes);
app.use('/api/user', friendRoutes);
app.use('/api/article', articleRoutes);
app.use('/api', recommendationRoutes);
app.use('/api', authenticationRoutes);


// Errorhandlers
const errors = require('./errors')

app.use('*', function(_, res) {
    res.status(404).end()
});

app.use('*', errors.handlers);

app.use('*', function(err, req, res, next) {
    console.warn(err.name);
    res.status(500).json({
        message: 'something really unexpected happened'
    })
})

// TODO: error handlers

module.exports = app;
