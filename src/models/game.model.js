const mongoose = require('mongoose');
const ReviewSchema = require('./review.schema');
const Schema = mongoose.Schema;
const getModel = require('./model_cache')

const GameSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A name is required.']
    },
    price: {
        type: Number,
        required: [true, 'A game needs a price.'],
        validate: {
            validator: (price) => price >= 0,
            message: 'A price needs to be 0 or positive.'
        }
    },
    short_description: {
        type: String,
        required: [true, 'A short_description is required.']
    },
    long_description: {
        type: String,
        required: [true, 'A long_description is required.']
    },
    release_date: {
        type: Date, 
        required: [true, 'A release_date is required.']
    },
    trailer_url: {
        type: String,
        required: [true, 'A trailer_url is required.']
    },
    banner: {
        type: String,
        required: false,
        default: ''
    },
    reviews: {
        type: [ReviewSchema],
        default: []
    },
    category: {
        type: String,
        required: [true, 'A category is required.'],
        validate: {
            validator: (value) => {
                return value === 'action' || value === 'RPG' || value === 'horror' || value === 'strategy' || value === 'adventure' || value === 'casual' || value === 'survival'
            },
            message: 'Gamecategory must be: action, RPG, horror, strategy, adventure, casual or survival!'
        }
    },
    publisher: {
        type: String, 
        required: [true, 'A publisher is required.']
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: [true, 'createdBy is required.'],
    }
});


module.exports = getModel('game', GameSchema);