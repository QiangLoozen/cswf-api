const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ListSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A name is required.']
    },
    description: {
        type: String,
        required: [true, 'A description is required.']
    },
    date: {
        type: Date,
        default: Date.now()
    },
    games: [{
        type: Schema.Types.ObjectId,
        ref: 'game',
        default: [],
        autopopulate: true
    }]
});

ListSchema.plugin(require('mongoose-autopopulate'));


module.exports = ListSchema;

