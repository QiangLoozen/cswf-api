const chai = require('chai');
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const User = require('./user.model')();

describe('list schema', function() {
    describe('unit tests', function() {
        let testUser;

        beforeEach((done) => {
            testUser = new User({lists: [{name: "list", description: "desc"}], username: 'Jane', email: 'email@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"});
            done();
        });

        it('should not reject', async function() {
            await expect(testUser.validate()).to.not.be.rejectedWith(Error);
        })
        
        it('should create an empty game list', async function() {
            const user = await testUser.save();
            expect(user.lists[0]).to.have.property('games').and.to.be.empty
        });
    
        it('should reject a missing name', async function() {
            testUser.lists[0].name = undefined;

            await expect(new User(testUser).validate()).to.be.rejectedWith(Error)
        });

        it('should reject a missing description', async function() {
            testUser.lists[0].description = undefined;

            await expect(new User(testUser).validate()).to.be.rejectedWith(Error)
        });

        it('should default date of creation', async function() {
            const user = new User(testUser);
            await user.save();

            expect(user.lists[0]).to.have.property('date').and.not.to.be.null
        })
    });
});