const mongoose = require('mongoose');
const ListSchema = require('./list.schema');
const Schema = mongoose.Schema;
const getModel = require('./model_cache')


const UserSchema = new Schema({
    username: {
        type: String,
        required: [true, 'A username is required.']
    },
    email: {
        type: String,
        required: [true, 'A email is required.'],
        unique: [true, 'Email already exist!'],
        match: [
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            "Invalid email!"
        ]
    },
    password: {
        type: String,
        required: [true, 'A password is required.'],
        minlength: [7, "Password needs to be at least 7 characters long"]
    },
    birthdate: {
        type: Date,
        required: [true, 'A birthdate is required.'],
        validate: {
            validator: (value) => {
                return value < Date.now();
            },
            message: 'Birthday has to be in the past!'
        }
    },
    role: {
        type: String,
        required: [true, 'A role is required.'],
        validate: {
            validator: (value) => {
                return value === 'customer' || value === 'creator'
            },
            message: 'role must be a customer or a creator!'
        }
    },
    lists: [{
        type: ListSchema,
        default: []
    }],
    games: [{
        type: Schema.Types.ObjectId,
        ref: 'game',
        default: [],
    }],
    profilePicture: {
        type: String,
        required: false,
        default: ''
    }
});

UserSchema.pre('remove', function(next) {
    const Game = mongoose.model('game')

    // Pull operator removes al matching items in the array
    Game.updateMany({}, {$pull: {'reviews': {'user': this._id}}})
        .then(() => next())
})

module.exports = getModel('user', UserSchema);