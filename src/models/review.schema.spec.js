const chai = require('chai')
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Game = require('./game.model')()

describe('review schema', function() {
    describe('unit tests', function() {
        let testGame;

        beforeEach((done) => {
            testGame = new Game({reviews: [{user: "61a76293aa00c73ff7434f30", title: "reviewtitle", content: "content", rating: 4}], createdBy: "61a76293aa00c73ff7434f30", name: "game1", price: 10, short_description: "sd", long_description: "ld", release_date: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), trailer_url: "tu", category: "action", publisher: "p"});
            done();
        });

        it('should not reject', async function() {
            await expect(testGame.validate()).to.not.be.rejectedWith(Error);
        })

        it('should reject a missing user', async function() {
            testGame.reviews[0].user = undefined
            await expect(new Game(testGame).validate()).to.be.rejectedWith(Error);
        })
        
        it('should reject a rating above 5', async function() {
            testGame.reviews[0].rating = 6;
            await expect(new Game(testGame).validate()).to.be.rejectedWith(Error);
        })

        it('should reject a negative rating', async function() {
            testGame.reviews[0].rating = -4;
            await expect(new Game(testGame).validate()).to.be.rejectedWith(Error);
        })
    
        it('should reject a missing content', async function() {
            testGame.reviews[0].content = undefined;
            await expect(new Game(testGame).validate()).to.be.rejectedWith(Error)
        })

        it('should default 0 likes', async function() {
            const newGame = new Game(testGame);
            await newGame.save()
            expect(newGame.reviews[0]).to.have.property("likes").and.to.be.empty
        })

        it('should default date of creation', async function() {
            const game = new Game(testGame);
            await game.save();

            expect(game.reviews[0]).to.have.property('date').and.not.to.be.null
        })
    })
})