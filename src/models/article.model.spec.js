const chai = require('chai')
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Article = require('./article.model')()

describe('article model', function() {
    describe('unit tests', function() {
        let testArticle;

        beforeEach((done) => {
            testArticle = new Article({user: "61a76293aa00c73ff7434f30", title: "article1", subtitle: "subtitle", content: "content", banner: "img.jpg", category: "news"});
            done();
        });

        it('should not reject', async function() {
            await expect(testArticle.validate()).to.not.be.rejectedWith(Error);
        })

        it('should reject an invalid category', async function() {
            testArticle.category = "invalidCategory",
            await expect(testArticle.validate()).to.be.rejectedWith(Error);
        })
    
        it('should reject a missing content', async function() {
            testArticle.content = undefined;
            await expect(testArticle.validate()).to.be.rejectedWith(Error)
        })

        it('should reject a missing user', async function() {
            testArticle.user = undefined;
            await expect(testArticle.validate()).to.be.rejectedWith(Error)
        })

        it('should default banner', async function() {
            testArticle.banner = undefined
            const article = new Article(testArticle)
            await article.save();

            expect(article).to.have.property('banner').and.to.be.empty
        })

        it('should default date of creation', async function() {
            testArticle.date = undefined;
            const article = new Article(testArticle);
            await article.save();

            expect(article).to.have.property('date').and.not.to.be.null
        })
    })
})