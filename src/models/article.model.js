const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const getModel = require('./model_cache')

const ArticleSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        required: [true, 'A user needs to ben attached to a review.'],
        ref: 'user',
        autopopulate: true
    },
    title: {
        type: String,
        required: [true, 'A title is required.']
    },
    subtitle: {
        type: String,
        required: [true, 'A subtitle is required.']
    },
    content: {
        type: String,
        required: [true, 'The content is required.']
    },
    banner: {
        type: String,
        required: false,
        default: ''
    },
    date: {        
        type: Date,
        required: false,
        default: Date.now()
    },
    category: {
        type: String,
        required: [true, 'A category is required.'],
        validate: {
            validator: (value) => {
                return value === 'news'
            },
            message: 'The category must be news'
        }
    }
});

ArticleSchema.plugin(require('mongoose-autopopulate'));


module.exports = getModel('article', ArticleSchema);