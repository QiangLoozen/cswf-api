const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReviewSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        required: [true, 'A user needs to ben attached to a review.'],
        ref: 'user',
        autopopulate: true
    },
    title: {
        type: String,
        required: [true, 'A title is required.']
    },
    content: {
        type: String,
        required: [true, 'The content is required.']
    },
    rating: {
        type: Number,
        required: [true, 'A rating is required.'],
        validate: {
            validator: (rating) => {
                return Number.isInteger(rating) && 0 <= rating && rating <= 5;
            },
            message: 'A rating can only be 1, 2, 3, 4, 5 stars'
        }
    },
    likes: [{
        type: Schema.Types.ObjectId,
        required: false,
        default: [],
        ref: 'user'
    }],
    date: {
        type: Date,
        required: false,
        default: Date.now()
    }
});


ReviewSchema.plugin(require('mongoose-autopopulate'));


module.exports = ReviewSchema;