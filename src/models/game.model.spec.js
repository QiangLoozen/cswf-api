const chai = require('chai')
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Game = require('./game.model')()

describe('game model', function() {
    describe('unit tests', function() {
        let testGame;

        beforeEach((done) => {
            testGame = new Game({banner: 'newBanner.jpg', createdBy: "61a76293aa00c73ff7434f30", name: "game1", price: 10, short_description: "sd", long_description: "ld", release_date: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), trailer_url: "tu", category: "action", publisher: "p"});
            done();
        });


        it('should not reject', async function() {
            await expect(testGame.validate()).to.not.be.rejectedWith(Error);
        })
        
        it('should reject a negative price', async function() {
            testGame.price = -100

            // use validate and not save to make it a real unit test (we don't require a db this way)
            await expect(new Game(testGame).validate()).to.be.rejectedWith(Error)
        })

        it('should reject a missing creator', async function() {
            testGame.createdBy = undefined;
            await expect(new Game(testGame).validate()).to.be.rejectedWith(Error)
        })
    
        it('should reject a missing price', async function() {
            testGame.price = undefined;

            await expect(new Game(testGame).validate()).to.be.rejectedWith(Error)
        })

        it('should reject a missing name', async function() {
            testGame.name = undefined;

            await expect(new Game(testGame).validate()).to.be.rejectedWith(Error)
        })

        it('should default banner', async function() {
            testGame.banner = undefined
            const game = new Game(testGame);
            await game.save();
            expect(game).to.have.property('banner').and.to.be.empty
        })

        it('should default reviews with empty list', async function() {
            const game = new Game(testGame);
            await game.save();
            expect(game).to.have.property('reviews').and.to.be.empty
        })
    })
})