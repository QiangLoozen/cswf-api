const chai = require('chai')
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const User = require('./user.model')() 

describe('user model', function() {
    describe('unit tests', function() {

        let testUser;

        beforeEach((done) => {
            testUser = new User({username: 'Jane', email: 'email@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"});
            done();
        });

        it('should not reject', async function() {
            await expect(testUser.validate()).to.not.be.rejectedWith(Error);
        })

        it('should reject a missing user name', async function() {
            const user = new User({})
    
            await expect(user.save()).to.be.rejectedWith(Error)
        })

        it('should reject a invalid email', async function() {
            testUser.email = "email.com"
            const user = new User(testUser)
    
            await expect(user.save()).to.be.rejectedWith(Error)
        })

        it('should reject a invalid password', async function() {
            testUser.password = "short"
            const user = new User(testUser)
    
            await expect(user.save()).to.be.rejectedWith(Error)
        })

        it('should reject a invalid birthdate', async function() {
            let today = new Date();
            today.setDate(today.getDate() + 100);
            testUser.birthdate = today; 
            const user = new User(testUser)
            await expect(user.save()).to.be.rejectedWith(Error)
        })

        it('should reject a invalid role', async function() {
            testUser.role = "invalidRole"; 
            const user = new User(testUser)
            await expect(user.save()).to.be.rejectedWith(Error)
        })
    
        it('should create an empty game list by default (BOUGHT)', async function() {
            const user = new User(testUser);
            await user.save()

            expect(user).to.have.property('games').and.to.be.empty
        })

        it('should create an empty game list by default (LIST)', async function() {
            const user = new User(testUser);
            await user.save()

            expect(user).to.have.property('lists').and.to.be.empty
        })

        it('should default profilepicture', async function() {
            const user = new User(testUser);
            await user.save()

            expect(user).to.have.property('profilePicture').and.to.be.empty
        })
    
        it('should not create duplicate emails', async function() {
            await new User({username: 'Jane', email: 'email@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"}).save()
            const user = new User({username: 'Jane', email: 'email@email.com', password: "password", birthdate: new Date(Date.parse("04 Dec 1995 00:12:00 GMT")), role: "customer"})
            
            await expect(user.save()).to.be.rejectedWith(Error)
    
            let count = await User.find().countDocuments()
            expect(count).to.equal(1)
        })
    })
})