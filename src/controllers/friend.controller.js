const errors = require('../errors');
const User = require('../models/user.model')();
const neo = require('../../neo');

async function getFriends(req, res) {
    // Check if users exist
    const user = await User.findById(req.params.id);

    if(!user) {
        console.log("Could not retrieve user")
        throw new errors.EntityNotFoundError("Could not retrieve user");
    }

    // Get friends in neo
    const session = neo.session();
    const result = await session.run(neo.getFriends, {
        userId: user._id.toString(),
    }).catch((err) => {console.log(err)})

    const friendIds = result.records[0].get('friendIds');
    const friends = await User.find({_id: {$in: friendIds}});
    res.status(200).json(friends);
}

async function getFriendRequests(req, res) {
    // Check if users exist
    const user = await User.findById(req.params.id);

    if(!user) {
        console.log("Could not retrieve user")
        throw new errors.EntityNotFoundError("Could not retrieve user");
    }

    // Get pending requests in neo
    const session = neo.session();
    const result = await session.run(neo.getFriendRequests, {
        userId: user._id.toString(),
    }).catch((err) => {console.log(err)})

    const pendingIds = result.records[0].get('pendingIds');
    const allRequests = await User.find({_id: {$in: pendingIds}});
    res.status(200).json(allRequests);
}

async function friendRequest(req, res) {
    // Check if users exist
    const user = await User.findById(req.params.id);
    const sender = await User.findById(req.body.senderId);

    if(!user || !sender) {
        console.log("Could not retrieve user or sender")
        throw new errors.EntityNotFoundError("Could not retrieve user or sender");
    }

    console.log(req.authId + " and " + sender._id.toString())
    if(req.authId != sender._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 

    // Add request to user
    const session = neo.session();
    await session.run(neo.sendFriendRequest, {
        userId: user._id.toString(),
        senderId: sender._id.toString()
    }).catch((err) => {console.log(err)})

    res.status(204).end()
}

async function acceptFriendRequest(req, res) {
    // Check if users exist
    const user = await User.findById(req.params.id);
    const sender = await User.findById(req.body.senderId);

    if(!user || !sender) {
        console.log("Could not retrieve user or sender")
        throw new errors.EntityNotFoundError("Could not retrieve user or sender");
    }

    console.log(req.authId + " and " + user._id.toString())
    if(req.authId != user._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 

    // Remove friendrequest first
    const session = neo.session();
    await session.run(neo.removeFriendRequest, {
        userId: user._id.toString(),
        senderId: sender._id.toString(),
    }).catch((err) => {console.log(err)})

    // Make friendship relation in neo
    await session.run(neo.addFriend, {
        userId: user._id.toString(),
        friendId: sender._id.toString()
    }).catch((err) => {console.log(err)})
    
    res.status(204).end();
}

async function declineFriendRequest(req, res) {
    // Check if users exist
    const user = await User.findById(req.params.id);
    const sender = await User.findById(req.body.senderId);

    if(!user || !sender) {
        console.log("Could not retrieve user or sender")
        throw new errors.EntityNotFoundError("Could not retrieve user or sender");
    }

    console.log(req.authId + " and " + user._id.toString())
    if(req.authId != user._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 

    // Remove friendrequest first
    const session = neo.session();
    await session.run(neo.removeFriendRequest, {
        userId: user._id.toString(),
        senderId: sender._id.toString(),
    }).catch((err) => {console.log(err)})

    res.status(204).end()
}

async function removeFriend(req, res) {
    
    const user = await User.findById(req.params.id);
    const friend = await User.findById(req.body.senderId);

    if(!user || !friend) {
        console.log("Could not retrieve user or sender")
        throw new errors.EntityNotFoundError("Could not retrieve user or sender");
    }

    console.log(req.authId + " and " + user._id.toString())
    if(req.authId != user._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 
   
    // remove friendship relation in neo
    const session = neo.session();
    await session.run(neo.removeFriend, {
        userId: user._id.toString(),
        friendId: friend._id.toString(),
    }).catch((err) => {console.log(err)})


    res.status(204).end()
}

module.exports = {
    getFriends,
    getFriendRequests,
    friendRequest,
    acceptFriendRequest,
    declineFriendRequest,
    removeFriend,
}