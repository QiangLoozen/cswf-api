const neo = require('../../neo');
const Game = require('../models/game.model')();
const User = require('../models/user.model')();

async function getRecommendations(query, req, res) {
    const session = neo.session();
    
    const result = await session.run(query, {
        userId: req.params.id,
    }).catch((err) => {console.log(err)})

    // we only expect 1 row with results, containing an array of product ids in the field 'productIds'
    // see the queries in neo.js for what is returned
    //console.log("gameIds: ", result.records)
    const gameIds = result.records.map((item) => {
        return item.get('gameIds').properties.id;
    });
    session.close()
    
    const recommendations = await Game.find({_id: {$in: gameIds}}).limit(10)
    
    res.status(200).json(recommendations)
}

async function getRecommendedFriends(query, req, res) {
    const session = neo.session();
    
    const result = await session.run(query, {
        userId: req.params.id}).catch((err) => {console.log(err)})
    
    const friendIds = result.records[0].get('friendIds');
    session.close()

    const recommendedFriends = await User.find({_id: {$in: friendIds}}).limit(10);
    res.status(200).json(recommendedFriends);
}

async function similarGames(req, res) {
    await getRecommendations(neo.recommendSimilar, req, res)
}

async function recommendedFriends(req, res) {
    await getRecommendedFriends(neo.recommendFriends, req, res)
}

module.exports = {
    similarGames,
    recommendedFriends
}