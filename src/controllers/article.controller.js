const Article = require('../models/article.model')(); // note we need to call the model caching function

const errors = require('../errors');

async function updateArticle(req, res) {
    const article = await Article.findById(req.params.id);
    console.log(req.authId + " and " + article.user._id.toString())
    if(req.authId != article.user._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 
    await article.update(req.body)
    res.status(204).end();
}
async function deleteArticle(req, res) {
    const article = await Article.findById(req.params.id);
    if(!article.user) {
        throw new errors.EntityNotFoundError("No user bound to article, authorization could not be checked");
    }
    console.log(req.authId + " and " + article.user._id.toString())
    if(req.authId != article.user._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 
    await article.delete();
    res.status(204).end();
}

module.exports = {
    updateArticle, 
    deleteArticle
}