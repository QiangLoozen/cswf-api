const errors = require('../errors');
const User = require('../models/user.model')();
const Game = require('../models/game.model')();

async function getLists(req, res) {
    const user = await User.findById(req.params.id);
    if(!user) {
        console.log("Could not find the user")
        throw new errors.EntityNotFoundError('Could not find the user');
    }

    res.status(200).send(user.lists);
}

async function createList(req, res) {
    const user = await User.findById(req.params.id);
    if(!user) {
        throw new errors.EntityNotFoundError('Could not find the user');
    }
    console.log(req.authId + " and " + user._id.toString())
    if(req.authId != user._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 

    user.lists.push(req.body);

    await user.save()
    res.status(201).send(user)
}

async function updateList(req, res) {
    const user = await User.findById(req.params.id);

    if(!user) {
        console.log("Could not find the user")
        throw new errors.EntityNotFoundError('Could not find the user');
    }

    console.log(req.authId + " and " + user._id.toString())
    if(req.authId != user._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 
    
    let index = -1;
    user.lists.forEach((item, i) => {
        if(item._id.toString() == req.params.listId) {
            index = i;
        }
    });

    if(index == -1) {
        console.log("Could not find the list")
        throw new errors.EntityNotFoundError('Could not find the list');
    }

    user.lists[index] = req.body
    await user.save()

    res.status(204).end()
}

async function deleteList(req, res) {
    const user = await User.findById(req.params.id);

    if(!user) {
        console.log("Could not find the user")
        throw new errors.EntityNotFoundError('Could not find the user');
    }
    
    console.log(req.authId + " and " + user._id.toString())
    if(req.authId != user._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 

    let index = -1;
    user.lists.forEach((item, i) => {
        if(item._id.toString() == req.params.listId) {
            index = i;
        }
    });

    if(index == -1) {
        console.log("Could not find the list")
        throw new errors.EntityNotFoundError('Could not find the list');
    }

    user.lists.splice(index, 1);

    await user.save()
    res.status(204).end();
}

async function addToList(req, res) {
    const user = await User.findById(req.params.id);
    const game = await Game.findById(req.params.gameId);

    if(!user || !game) {
        console.log("Could not find the user or game")
        throw new errors.EntityNotFoundError('Could not find the user or game');
    }

    console.log(req.authId + " and " + user._id.toString())
    if(req.authId != user._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 
    
    let index = -1;
    user.lists.forEach((item, i) => {
        if(item._id.toString() == req.params.listId) {
            index = i;
        }
    });

    if(index == -1) {
        console.log("Could not find the list")
        throw new errors.EntityNotFoundError('Could not find the list');
    }

    user.lists[index].games.push(game._id)


    await user.save()
    res.status(201).send(user)
}

async function removeFromList(req, res) {
    const user = await User.findById(req.params.id);
    const game = await Game.findById(req.params.gameId);

    if(!user || !game) {
        console.log("Could not find the user or game")
        throw new errors.EntityNotFoundError('Could not find the user or game');
    }

    console.log(req.authId + " and " + user._id.toString())
    if(req.authId != user._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 
    
    let index = -1;
    user.lists.forEach((item, i) => {
        if(item._id.toString() == req.params.listId) {
            index = i;
        }
    });

    if(index == -1) {
        console.log("Could not find the list")
        throw new errors.EntityNotFoundError('Could not find the list');
    } 

    user.lists[index].games.forEach((item, i) => {
        if(item._id.toString() === game._id.toString()) {
            gameIndex = i;
        }
    });

    if(gameIndex == -1) {
        console.log("Could not find the game")
        throw new errors.EntityNotFoundError('Could not find the game');
    } 

    user.lists[index].games.splice(gameIndex, 1)

    await user.save()
    res.status(204).end()
}

module.exports = {
    getLists,
    createList,
    updateList,
    deleteList,
    addToList,
    removeFromList
}