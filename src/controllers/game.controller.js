const Game = require('../models/game.model')(); // note we need to call the model caching function
const User = require('../models/user.model')(); // note we need to call the model caching function

const neo = require('../../neo');
const errors = require('../errors');

async function purchase(req, res) {
    if(!req.body.userId) {
        throw new errors.EntityNotFoundError('User is required to purchase a product');
    }

    // Fetching game and user data
    console.log("params:", req.params)
    const game = await Game.findById(req.params.id);
    if(!game) {
        console.log("Could not retrieve game")
        throw new errors.EntityNotFoundError("Could not retrieve game");    }

    const user = await User.findById(req.body.userId);

    user.games.push(game._id);
    await user.save();

    const session = neo.session()

    console.log(`Established connection with user: ${user.username} and game ${game.name}`)

    session.run(neo.purchase, {
        gameId: game._id.toString(),
        userId: user._id.toString(),
        category: game.category
    })
    .then(() => {

        session.close()

        res.status(201).end()
    })
    .catch(err => {
        console.log(err);
        session.close()
    });

}

async function updateGame(req, res) {
    const game = await Game.findById(req.params.id);
    console.log(req.authId + " and " + game.createdBy._id.toString())
    if(req.authId != game.createdBy._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 
    await game.update(req.body);
    res.status(204).end()
}

async function deleteGame(req, res) {
    const game = await Game.findById(req.params.id)

    if(!game) {
        throw new errors.EntityNotFoundError('could not retrieve game');
    }
    if(!game.createdBy) {
        throw new errors.EntityNotFoundError("No user bound to article, authorization could not be checked");
    }

    console.log(req.authId + " and " + game.createdBy._id.toString())
    if(req.authId != game.createdBy._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 

    await game.delete()

    const session = neo.session();

    await session.run(neo.deleteGame, {
        gameId: req.params.id,
    }).catch((err) => {console.log(err)})

    res.status(204).end()
}

async function getGame(req, res) {
    const game = await Game.findById(req.params.id).populate('reviews.user').populate('createdBy');
    res.status(200).send(game)
}

async function createReview(req, res) {
    const game = await Game.findById(req.params.id);

    if(!game) {
        console.log("Could not retrieve game")
        throw new errors.EntityNotFoundError("Could not retrieve  game");
    }

    game.reviews.push(req.body)
    await game.save();

    res.status(201).send(game.reviews);
}

async function editReview(req, res) {
    const game = await Game.findById(req.params.id);
    const reviewId = req.params.reviewId

    if(!game || !reviewId) {
        console.log("Could not retrieve user or game or review")
        throw new errors.EntityNotFoundError("Could not retrieve user or game or review");
    }

    let index = -1
    game.reviews.forEach((r, i) => {
        if(r._id.toString() == reviewId) {
            index = i;
        }
    });

    if(index == -1) {
        console.log("Could not retrieve the review")
        throw new errors.EntityNotFoundError("Could not retrieve the review");
    }

    game.reviews[index] = req.body
    await game.save();

    res.status(204).send();
}

async function deleteReview(req, res) {
    const game = await Game.findById(req.params.id);
    const reviewId = req.params.reviewId

    if(!game || !reviewId) {
        console.log("Could not retrieve game or review ")
        throw new errors.EntityNotFoundError("Could not retrieve game or review");
    }

    let index = -1
    game.reviews.forEach((r, i) => {
        if(r._id.toString() == reviewId) {
            index = i;
        }
    });

    if(index == -1) {
        console.log("Could not retrieve the review")
        throw new errors.EntityNotFoundError("Could not retrieve the review");
    }

    game.reviews.splice(index, 1);
    await game.save()

    res.status(204).send();
}

async function addLike(req, res) {
    const game = await Game.findById(req.params.id);
    const user = await User.findById(req.body.userId);
    const reviewId = req.params.reviewId;

    if(!game || !user || !reviewId) {
        console.log("Could not retrieve user or game or review")
        throw new errors.EntityNotFoundError("Could not retrieve user or game or review");
    }

    let index = -1

    game.reviews.forEach((review, i) => {
        if(review._id.toString() == reviewId) {
            index = i;
        }
    });

    if(index == -1) {
        console.log("Could not retrieve the review")
        throw new errors.EntityNotFoundError("Could not retrieve the review");
    }

    game.reviews[index].likes.push(user._id);
    await game.save();

    res.status(204).send()

}
async function removeLike(req, res) {
    const game = await Game.findById(req.params.id);
    const user = await User.findById(req.body.userId);
    const reviewId = req.params.reviewId;

    if(!game || !user || !reviewId) {
        console.log("Could not retrieve user or game or review")
        throw new errors.EntityNotFoundError("Could not retrieve user or game or review");
    }

    let index = -1

    game.reviews.forEach((review, i) => {
        if(review._id.toString() == reviewId) {
            index = i;
        }
    });

    if(index == -1) {
        console.log("Could not retrieve the review")
        throw new errors.EntityNotFoundError("Could not retrieve the review");
    }

    let likeIndex = -1;
    game.reviews[index].likes.forEach((like, i) => {
        if(like.toString() == user._id.toString()) {
            likeIndex = i;
        }
    });

    if(likeIndex == -1) {
        console.log("Could not retrieve the like")
        throw new errors.EntityNotFoundError("Could not retrieve the like");
    }
    
    game.reviews[index].likes.splice(likeIndex, 1);
    await game.save();

    res.status(204).send()
}

module.exports = {
    getGame,
    deleteGame,
    purchase,
    createReview,
    editReview,
    deleteReview,
    addLike,
    removeLike,
    updateGame,
    deleteGame
}