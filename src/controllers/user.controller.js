const User = require('../models/user.model')();
const errors = require('../errors');
const neo = require('../../neo');


async function getAllEmails(req, res) {
     let emails = await User.find();
     const result = emails.map((item) => {
        return item.email
     })

     if (result) {
        res.status(200).send(result);
     } else {
        throw new errors.EntityNotFoundError('could not retrieve emails');
     }
}

async function updateUser(req, res) {
    const user = await User.findById(req.params.id);
    if(!user) {
        throw new errors.EntityNotFoundError("Could not retrieve user");
    }

    console.log(req.authId + " and " + user._id.toString())
    if(req.authId != user._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 
    await user.update(req.body)
    res.status(204).end()
}

async function deleteUser (req, res, next) {
    const user = await User.findById(req.params.id)

    if(!user) {
        throw new errors.EntityNotFoundError('could not retrieve user');
    }

    console.log(req.authId + " and " + user._id.toString())
    if(req.authId != user._id.toString()) {
        throw new errors.AuthenticationError("Unauthorized action.");
    } 

    await user.delete()

    const session = neo.session();

    await session.run(neo.deleteUser, {
        userId: req.params.id,
    }).catch((err) => {console.log(err)})

    res.status(204).end()
}

async function getUser (req, res) {
    const user = await User.findById(req.params.id).populate('games').populate('lists.games');

    if(!user) {
        throw new errors.EntityNotFoundError('could not retrieve user');
    }

    res.status(200).send(user)
}

module.exports = {
    deleteUser,
    updateUser,
    getAllEmails,
    getUser
}