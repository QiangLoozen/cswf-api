const User = require('../models/user.model')();
const jwt = require("jsonwebtoken");
const errors = require('../errors');

const bcrypt = require("bcrypt");
const saltRounds = 10;


async function register(req, res, next) {
    const newUser = new User(req.body)
    bcrypt.hash(newUser.password, saltRounds).then(async (hash) => {
        newUser.password = hash;

        await newUser.save().catch((err) => {
            next(err);
        });

        const payload = {
            _id: newUser._id.toString()
        }


        jwt.sign(payload, process.env.JWT_SECRET, (err, result) => {
            if(err) {
                next(err)
            }

            let mappedUser = {...newUser._doc, token: result};
            delete mappedUser.password;

            res.status(201).json(mappedUser);
        })
    })

    
}

async function login(req, res) {
    if(!req.body.email) {
        throw new errors.EntityNotFoundError('User email is required to login');
    } else if (!req.body.password) {
        throw new errors.EntityNotFoundError('Password is required to login');
    }
    const user = await User.findOne({email: req.body.email}).populate('games').populate('lists.games');

    if(!user) {
        throw new errors.EntityNotFoundError('Email does not exist!');
    }
    bcrypt.compare(req.body.password, user.password).then((result) => {
        if (result) {
            const payload = {
                _id: user._id.toString()
            }
        
            jwt.sign(payload, process.env.JWT_SECRET, (err, result) => {
                if(err) {
                    next(err)
                }
                let mappedUser = {...user._doc, token: result};
                delete mappedUser.password;
                res.status(200).json(mappedUser)
            })
        } else {
            res.status(400).json({message: "Wrong password!"})
        }
    })
}

async function validateToken(req, res, next) {
    const authHeader = req.headers.authorization;
    if(!authHeader) {
        throw new errors.AuthenticationError("No authorization header found.");
    }

    const token = authHeader.substring(7, authHeader.length);
    jwt.verify(token, process.env.JWT_SECRET, (err, result) => {
        if(err) {
            console.log(err)
            next(new errors.AuthenticationError("Invalid token"));
        }

        if(result) {
            req.authId = result._id
            next()
        }
    });
    
}

module.exports = {
    register,
    login,
    validateToken
}
